#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 12:57:11 2020

@author: Diogo Nardelli Siebert"""

import pandas as pd
import matplotlib.pylab as plt
import numpy as np 

# Abri arquivo de coordendas
dfCoord = pd.read_csv("coordinate.msh", delim_whitespace = True , names = ["x","y","z"] )
dfCoord["z"].fillna(0,inplace=True) #as vezes o arquivo 2D não tem a coordenada z

# Arqui o arquivo de labels
dfLabel = pd.read_csv("label.txt", delim_whitespace = True , names = ["index", "label"] )

# Abri o arquivo da topologia da malha
f = open("topo.msh")
mesh = [ np.array( row.rstrip().lstrip().split(" ")[1:], dtype = int ) for row in f]
f.close()

# Abri o arquivo da topologia do contorno
f = open("bctopo.msh")
bc = [ {"topo" : np.array( row.rstrip().lstrip().split(" ")[1:-1], dtype = int ), "type" : dfLabel["label"][ dfLabel["index"] == int(row.rstrip().lstrip().split(" ")[-1]) ].values[0] }  for row in f]
f.close()

keys = dfLabel["label"].to_list() + ["BOTTOM","TOP","VOLUME" ]
dim  = len( dfLabel["label"] ) * [2] + [2,2,3]
dictPhysical = { k : n + 1 for n,k in list( enumerate(keys) ) } 

# def getSurfaceTag( edge ):
#     machBoundary = list( filter( lambda x : set(x["topo"])  ==  set(edge)  , bc ) )
#     if ( machBoundary == [] ):
#         return [0, len( dfLabel ) + 4]
#     else:
#         return [ dictPhysical[ machBoundary[0]["type"] ] , dictPhysical[ machBoundary[0]["type"] ]  ]

N = len(dfCoord)
newMesh = []
for element in mesh:
    
    # Sou um trinângulo?
    if len(element) == 3:
        
        element = element + 1
        # Transforma em um prisma
        newElement = {}
        newElement["type"] = 6
        newElement["nodes"] = np.concatenate( (element , element + N ) )
        newElement["tags"] = [ dictPhysical["VOLUME" ] , len( dfLabel ) + 1]
        newMesh.append(newElement)
        
        
        # Adiciona faces triangulares
        
        newElement = {}
        newElement["type"] = 2
        newElement["nodes"] = element
        newElement["tags"] = [ dictPhysical["BOTTOM" ] , len( dfLabel ) + 2]
        newMesh.append(newElement)
        newElement = {}
        newElement["type"] = 2
        newElement["nodes"] = element + N
        newElement["tags"] = [ dictPhysical["TOP" ] , len( dfLabel ) + 3]
        newMesh.append(newElement)
        
        # # Adiciona faces quadriangulares
        
        # newElement = {}
        # newElement["type"] = 3
        # newElement["nodes"] = np.array( [ element[0], element[1] , element[1] + N, element[0] + N ] )
        # newElement["tags"] =  getSurfaceTag( element[0:2] )
        # newMesh.append(newElement)
        # newElement = {}
        # newElement["type"] = 3
        # newElement["nodes"] = np.array( [ element[1], element[2] , element[2] + N, element[1] + N ] )
        # newElement["tags"] =  getSurfaceTag( element[1:3] )
        # newMesh.append(newElement)
        # newElement = {}
        # newElement["type"] = 3
        # newElement["nodes"] =  np.array( [ element[2], element[0] , element[0] + N, element[2] + N ])
        # newElement["tags"] =  getSurfaceTag( element[0:3:2] )
        # newMesh.append(newElement)

    # Sou um quadrado?        
    elif len(element) == 4:
        
        element = element + 1
        # Transforma em um prisma
        newElement = {}
        newElement["type"] = 5
        newElement["nodes"] = np.concatenate( (element , element + N ) )
        newElement["tags"] = [ dictPhysical["VOLUME" ] , len( dfLabel ) + 1]
        newMesh.append(newElement)
             
        # Adiciona faces quadrangulares (inferior/superior)
        
        newElement = {}
        newElement["type"] = 3
        newElement["nodes"] = element
        newElement["tags"] = [ dictPhysical["BOTTOM" ] , len( dfLabel ) + 2]
        newMesh.append(newElement)
        newElement = {}
        newElement["type"] = 3
        newElement["nodes"] = element + N
        newElement["tags"] = [ dictPhysical["TOP" ] , len( dfLabel ) + 3]
        newMesh.append(newElement)

        # newElement = {}
        # newElement["type"] = 3
        # newElement["nodes"] = np.array( [ element[0], element[1] , element[1] + N, element[0] + N ] )
        # newElement["tags"] =  getSurfaceTag( element[0:2] )
            
        # newMesh.append(newElement)
        # newElement = {}
        # newElement["type"] = 3
        # newElement["nodes"] = np.array( [ element[1], element[2] , element[2] + N, element[1] + N ] )
        # newElement["tags"] =  getSurfaceTag( element[1:3] )
        # newMesh.append(newElement)
        # newElement = {}
        # newElement["type"] = 3
        # newElement["nodes"] = np.array( [ element[2], element[3] , element[3] + N, element[2] + N ] )
        # newElement["tags"] =  getSurfaceTag( element[2:4] )
        # newMesh.append(newElement)
        # newElement = {}
        # newElement["type"] = 3
        # newElement["nodes"] =  np.array( [ element[3], element[0] , element[0] + N, element[3] + N ])
        # newElement["tags"] =  getSurfaceTag( element[0:4:3] )
        # newMesh.append(newElement)

for j in range(len(bc)):
    
    element = bc[j]["topo"]+1
    newElement = {}
    newElement["type"] = 3
    newElement["nodes"] = np.array( [ element[0], element[1] , element[1] + N, element[0] + N ] )
    newElement["tags"] = [ dictPhysical[bc[j]["type"]] , dictPhysical[bc[j]["type"]]]
    newMesh.append(newElement)


f = open("converted.msh", "w")        
f.write("""$MeshFormat
{0} {1} {2}
$EndMeshFormat
""".format("2.2",0,8) ) 

f.write("""$PhysicalNames
{0}
""".format(len(keys)) )
for k,D in zip( keys,dim ):
    f.write("{0} {1} \"{2}\"\n".format( D, dictPhysical[k], k ) )
f.write("""$EndPhysicalNames
""")
    

f.write("""$Nodes
{0}
""".format(2*N) )

for index, row in dfCoord.iterrows():
    f.write( "{0} {x} {y} {z}\n".format(index+1, **row) )
    
for index, row in dfCoord.iterrows():
    f.write( "{0} {x} {y} 1\n".format(index+N+1, **row) )

f.write("""$EndNodes
""")


f.write("""$Elements
{0}
""".format( len(newMesh) ) )

for n, dictElement in enumerate( sorted( newMesh, key = lambda x : x["type"] ) ):
    f.write("""{0} {1} {2} {3} {4}
""".format( n+1, dictElement["type"] , len(dictElement["tags"]) , " ".join( map( str, dictElement["tags"] ) ) , " ".join( map( str, dictElement["nodes"] ) )  ) )  

f.write("""$EndElements
""")


f.close()
        
        
        